import «ArEquiv».Basic

import Mathlib.Tactic.Basic

infix:10 " ≡ " => Iff              -- Add ≡ notation, behaves as ↔ but meta
notation:100 " ⊤ " => True         -- ⊤ for True
notation:100 " ⊥ " => False        -- ⊥ for False
infix:10 " & " => And.intro        -- Infix operator for building conjunctions
postfix:20 " | _" => Or.inl        -- Building disjunctions from left option
prefix:20 "_ | " => Or.inr         -- Building disjunctions from right option
notation:10 a " -> " b => λ a => b -- Build implications via λ
notation:10 a "-> ⊥" => by intro a; contradiction -- Build negation by citing
                                                  --  evident contradiction

-- Proof solvers in general are based on intuitionistic logic, and thus omit
-- the classical axiom of the excluded middle. We can add it manually:
axiom ax_excl_middle : (p : Prop) → p ∨ ¬p

-- Double negation elimination would be equivalent:
-- axiom ax_double_negation_elim : (p : Prop) → ¬¬p → p
-- But L.E.M. tends to be easier to work with =)

-- 2.3.2.1ab
lemma double_negation_intro {p : Prop} : p ≡ ¬¬p := by
  constructor      -- Construct a proof for ≡ by proving both ways separately

  intro have_p                -- Proof of p ⊢ ¬¬p
  intro have_not_p            -- If we assume ¬p
  contradiction               --   we have a contradiction with p

  intro not_untrue            -- Proof of ¬¬p ⊢ p
  cases ax_excl_middle p with -- List options for p
   | inl have_p =>            -- ... if true,
     exact have_p             --     we're done
   | inr not_p =>             -- ... else, we have both ¬p and ¬¬p,
     contradiction            --     which is a contradiction

-- 2.3.2.1ac
lemma and_idem {p : Prop} : p ≡ p ∧ p := by
  constructor

  intro have_p            -- Proof of p ⊢ p ∧ p
  exact have_p & have_p   -- Just repeat p

  intro p_and_p           -- Proof of p ∧ p ⊢ p
  exact p_and_p.left      -- Pick either one

-- 2.3.2.1ad
lemma or_idem {p : Prop} : p ≡ p ∨ p := by
  constructor

  intro have_p         -- Proof of p ⊢ p ∨ p
  exact have_p | _     -- Pick either one

  intro p_or_p         -- Proof of p ∨ p ⊢ p
  cases p_or_p with    -- Two scenarios, both equal
    | inl have_p => exact have_p
    | inr have_p => exact have_p

-- 2.3.2.1ae
lemma falsity_disjunction {p : Prop} : p ≡ p ∨ ⊥ := by
  constructor

  exact have_p -> have_p | _ -- Proof of p ⊢ p ∨ ⊥: just take the p

  intro p_or_absurd               -- Proof of p ∨ ⊥ ⊢ p
  cases p_or_absurd with
    | inl have_p => exact have_p  -- In case of p, we either have p
    | inr absurd => contradiction -- or false, in which case everything goes

-- 2.3.2.1af
lemma truth_conjunction {p : Prop} : p ≡ p ∧ ⊤ := by
  constructor

  intro have_p              -- Proof of p ⊢ p ∧ ⊤
  exact have_p & True.intro -- Keep the p, add some always-available truth

  intro p_and_truth         -- Proof of p ∧ ⊤ ⊢ p
  exact p_and_truth.left    -- We can keep the p

-- 2.3.2.2
lemma or_commute {p q : Prop} : p ∨ q ≡ q ∨ p := by
  constructor

  intro p_or_q                              -- Proof of p ∨ q ⊢ q ∨ p
  cases p_or_q with
    | inl have_p => exact      _ | have_p   -- Either we have p, swap it to 2nd
    | inr have_q => exact have_q | _        --  or we have q, and swap it to 1st

  intro q_or_p                              -- Proof of q ∨ p ⊢ p ∨ q is exactly
  cases q_or_p with                         --  the same with the letters swapped
    | inl have_q => exact      _ | have_q
    | inr have_p => exact have_p | _

-- 2.3.2.3
lemma and_commute {p q : Prop} : p ∧ q ≡ q ∧ p := by
  constructor

  intro p_and_q                                    -- Proof of p ∧ q ⊢ q ∧ p
  cases p_and_q with
    | intro have_p have_q => exact have_q & have_p -- Extract both, swap

  intro q_and_p                                    -- Proof of q ∧ p ⊢ p ∧ q
  cases q_and_p with
    | intro have_q have_p => exact have_p & have_q --  same, different letters

-- 2.3.2.4ab
lemma iff_commute {p q : Prop} : p ↔ q ≡ q ↔ p := by
  constructor

  intro p_iff_q              -- Proof of p ↔ q ⊢ q ↔ p
  cases p_iff_q with         --  split into p → q and q → p, put back swapped
    | intro p_to_q q_to_p => exact Iff.intro q_to_p p_to_q

  intro q_iff_p              -- Proof of q ↔ p ⊢ p ↔ q
  cases q_iff_p with
    | intro q_to_p p_to_q => exact Iff.intro p_to_q q_to_p

-- 2.3.2.4ac
lemma iff_split {p q : Prop} : p ↔ q ≡ (p → q) ∧ (q → p) := by
  constructor

  intro p_iff_q        -- Proof of p ↔ q ⊢ (p → q) ∧ (q → p)
  cases p_iff_q with   --  split as above, repackage in ∧
    | intro p_to_q q_to_p => exact p_to_q & q_to_p

  intro two_impls      -- Proof of (p → q) ∧ (q → p) ⊢ p ↔ q
  cases two_impls with --  split from ∧, repackage in ↔
    | intro p_to_q q_to_p => exact Iff.intro p_to_q q_to_p

-- 2.3.2.4ad
lemma iff_equiv {p q : Prop} : p ↔ q ≡ (p ∧ q) ∨ (¬p ∧ ¬q) := by
  constructor

  intro iff                            -- Proof of p ↔ q ⊢ (p ∧ q) ∨ (¬p ∧ ¬q)
  have p_or_neg_p := ax_excl_middle p    -- p and q are either true or false
  have q_or_neg_q := ax_excl_middle q

  cases p_or_neg_p with
    | inl have_p =>
      cases q_or_neg_q with
      | inl have_q => exact      (have_p & have_q) | _ -- If both true left or
      | inr not_q =>                                   -- q and ¬p:
        have have_q : q := iff.mp have_p               --   q → p, so p
        contradiction                                  --   which cannot be
    | inr not_p =>
      cases q_or_neg_q with
      | inl have_q =>                                  -- p and ¬q
        have have_p : p := iff.mpr have_q              --   p → q, so q
        contradiction                                  --   which cannot be
      | inr not_q => exact       _ | (not_p & not_q)   -- Both false: right or

  intro both_or_neither       -- Proof of (p ∧ q) ∨ (¬p ∧ ¬q) ⊢ p ↔ q
  cases both_or_neither with
    | inl both =>
      cases both with         -- Both true satisfies p → q and q → p via _ → ⊤
        | intro have_p have_q => exact Iff.intro (_p -> have_q) (_q -> have_p)
    | inr neither =>
      cases neither with      -- Neither true satisfies via ⊥ → _
        | intro not_p not_q => exact Iff.intro (had_p -> ⊥) (had_q -> ⊥)
                              -- having either true would contradict

-- 2.3.2.5
lemma or_associative {p q r : Prop} : (p ∨ q) ∨ r ≡ p ∨ (q ∨ r) := by
  constructor

  intro left_brackets       -- Proof of (p ∨ q) ∨ r ⊢ p ∨ (q ∨ r)
  cases left_brackets with  --  find the one thats true
    | inl p_or_q =>
      cases p_or_q with
        | inl have_p => exact         have_p | _
        | inr have_q => exact              _ | (have_q | _)
    | inr have_r =>     exact              _ | (_ | have_r)

  intro right_brackets      -- Proof of p ∨ (q ∨ r) ⊢ (p ∨ q) ∨ r
  cases right_brackets with
    | inl have_p =>     exact   (have_p | _) | _
    | inr q_or_r =>
      cases q_or_r with
        | inl have_q => exact   (_ | have_q) | _
        | inr have_r => exact              _ | have_r

-- 2.3.2.6
lemma and_associative {p q r : Prop} : (p ∧ q) ∧ r ≡ p ∧ (q ∧ r) := by
  constructor

  intro left_brackets       -- Proof of (p ∧ q) ∧ r ⊢ p ∧ (q ∧ r)
  cases left_brackets with  --  unpack all and repack
    | intro p_and_q have_r =>
      cases p_and_q with
        | intro have_p have_q => exact have_p & (have_q & have_r)

  intro right_brackets      -- Proof of p ∧ (q ∧ r) ⊢ (p ∧ q) ∧ r
  cases right_brackets with
    | intro have_p q_and_r =>
      cases q_and_r with
        | intro have_q have_r => exact (have_p & have_q) & have_r

-- 2.3.2.7ab
lemma impl_intro {p q : Prop} : ¬p ∨ q ≡ p → q := by
  constructor

  intro as_disj                                    -- Proof of p → q ⊢ ¬p ∨ q
  cases as_disj with
    | inl not_p =>  exact (need_p -> ⊥)            -- If ¬p, q can be anything
    | inr have_q => exact _ -> have_q              --   if q, p is irrelevant

  intro p_to_q                                     -- Proof of p → q ⊢ ¬p ∨ q
  have p_or_neg_p := ax_excl_middle p
  cases p_or_neg_p with                            -- Either p is true
    | inl have_p => exact      _ | (p_to_q have_p) --   so q must be as well
    | inr not_p  => exact  not_p | _               -- or ¬p is true

-- 2.3.2.7ac
lemma contrapose {p q : Prop} : p → q ≡ ¬q → ¬p := by
  constructor

  intro p_to_q                           -- p → q ⊢ ¬q → ¬p
  intro not_q                            -- We have ¬q and p → q
  intro had_p                            -- If we had p
  have have_q := p_to_q had_p            -- we'd have q
  contradiction                          -- which is a contradiction

  intro contraposition                   -- ¬q → ¬p ⊢ p → q
  intro had_p                            -- If we had p
  have q_or_neg_q := ax_excl_middle q    -- and q must be either true or false
  cases q_or_neg_q with
    | inl have_q => exact have_q         -- then either q is true
    | inr not_q =>                       -- or we have not q
      have not_p := contraposition not_q -- and we can get ¬p
      contradiction                      -- which is a contradiction

-- 2.3.2.8
lemma nonimpl {p q : Prop} :  ¬(p → q) ≡ p ∧ ¬q := by
  constructor

  intro neg_impl                               -- Proof of ¬(p → q) ⊢ p ∧ ¬q

  have have_p := by                            -- Proof that we can provide p
    have p_or_neg_p := ax_excl_middle p
    cases p_or_neg_p with                      -- We have either p or ¬p
      | inl have_p => exact have_p             -- If we have p, that's done
      | inr not_p =>
        have : p → q := by exact (have_p -> ⊥) -- Else, we can construct p → q
        contradiction                          --  contradicting our assumption

  have not_q := by                             -- Proof for ¬q
    have q_or_neg_q := ax_excl_middle q
    cases q_or_neg_q with                      -- We have either q or ¬q
      | inl have_q =>                          -- If we have q
        have : p → q := by exact _ -> have_q   --  we can construct p → q
        contradiction                          --  contradicting our assumption
      | inr not_q => exact not_q               -- If not, we have ¬q as asked

  exact have_p & not_q                         -- Thus, we have both

  intro have_p_no_q                            -- Proof of p ∧ ¬q ⊢ ¬(p → q)
  cases have_p_no_q with
   | intro have_p not_q =>                     -- Separate terms p and ¬q
     intro if_p_to_q                           -- Then we can assume p → q
     have would_have_q := if_p_to_q have_p     --  which would give q and ¬q
     contradiction                             --  so we p → q cannot be true

-- 2.3.2.9
lemma deMorgan1 {p q : Prop} : ¬(p ∨ q) ≡ ¬p ∧ ¬q := by
  constructor

  intro neither_p_q                   -- Proof of ¬(p ∨ q) ⊢ ¬p ∧ ¬q

  have not_p : ¬p := by               -- p cannot be true
    intro had_p                       --  because if it were
    have either : p ∨ q := had_p | _  --  we could construct p ∨ q
    contradiction                     --  which contradicts the premisse

  have not_q :¬q := by                -- q cannot be true via exact same
    intro had_q                       --  reasoning
    have either : p ∨ q := _ |  had_q
    contradiction

  exact not_p & not_q                 -- With both ¬p and ¬q, construct ¬p ∧ ¬q

  intro both_not                      -- Proof of ¬p ∧ ¬q ⊢ ¬(p ∨ q)
  cases both_not with
    | intro not_p not_q =>            -- Split premisse into ¬p and ¬q
      intro had_p_or_q                -- Show we cannot prove p ∨ q by assuming
      cases had_p_or_q with           --  it is true, leading to contradiction
        | inl had_p => contradiction  -- In case we have p, we contradict ¬p
        | inr had_q => contradiction  --  and if we have q, we contradict ¬q

-- 2.3.2.10
lemma deMorgan2 {p q : Prop} : ¬(p ∧ q) ≡ ¬p ∨ ¬q := by
  constructor

  intro not_both_p_q                            -- Proof of ¬(p ∧ q) ⊢ ¬p ∨ ¬q
  have p_or_neg_p := ax_excl_middle p           -- Both p is either true or not
  have q_or_neg_q := ax_excl_middle q           --  and so is q
  cases p_or_neg_p with
    | inl have_p =>                             -- If p is true
      cases q_or_neg_q with
        | inl have_q =>                         --  and q is true
          have both := by exact have_p & have_q -- Then we can construct p ∧ q
          contradiction                         --  which contradicts ¬(p ∧ q)
        | inr not_q => exact      _ | not_q     -- If ¬q we have whatever ∨ ¬q
    | inr not_p =>     exact  not_p | _         -- If ¬p we have ¬p ∨ whatever

  intro either_not                              -- Proof of ¬p ∨ ¬q ⊢ ¬(p ∧ q)
  intro had_p_or_q                              -- Assume we had p ∧ q
  cases had_p_or_q with                         --  and find a contradiction
    | intro had_p had_q =>                      -- Assuming p ∧ q gives p and q
      cases either_not with                     -- Now, no matter if p or q or
        | inl not_p => contradiction            --  both is false, we contradict
        | inr not_q => contradiction            --  both being true.

-- 2.3.2.11
lemma and_or_dist {p q r : Prop} : (p ∨ q) ∧ r ≡ (p ∧ r) ∨ (q ∧ r) := by
  constructor

  intro either_and                  -- Proof of (p ∨ q) ∧ r ⊢ (p ∧ r) ∨ (q ∧ r)
  cases either_and with
    | intro p_or_q have_r =>                      -- Split into p ∨ q and r
      cases p_or_q with                           -- Examine cases of p ∨ q
      | inl have_p => exact (have_p & have_r) | _ -- If p construct p ∧ r
      | inr have_q => exact _ | (have_q & have_r) -- If q construct q ∧ r

  intro and_either_and              -- Proof of (p ∧ r) ∨ (q ∧ r) ⊢ (p ∨ q) ∧ r
  cases and_either_and with         -- Examine which conjunction holds
    | inl p_and_r =>                -- If p ∧ r is true
      cases p_and_r with            --  extract p and reconstruct rhs
        | intro have_p have_r => exact (have_p | _) & have_r
    | inr q_and_r =>                -- If q ∧ r is true
      cases q_and_r with            --  extract q and reconstruct rhs
        | intro have_q have_r => exact (_ | have_q) & have_r

-- 2.3.2.12
lemma or_and_dist {p q r : Prop} : (p ∧ q) ∨ r ≡ (p ∨ r) ∧ (q ∨ r) := by
  constructor

  intro both_or                      -- Proof of (p ∧ q) ∨ r ⊢ (p ∨ r) ∧ (q ∨ r)
  cases both_or with
    | inl p_and_q =>                 -- If we have p ∧ q
      cases p_and_q with             -- Split and use both to build disjunctions
        | intro have_p have_q => exact (have_p | _ & have_q | _)
    | inr have_r =>                  -- If r, build both disjunctions with it
        exact (_ | have_r & _ | have_r)

  intro either_and_either            -- Proof of (p ∨ r) ∧ (q ∨ r) ⊢ (p ∧ q) ∨ r
  cases either_and_either with
    | intro p_or_r q_or_r =>                        -- Extract p ∨ r and q ∨ r
      cases p_or_r with
      | inl have_p =>                               -- If we have p
        cases q_or_r with
        | inl have_q => exact (have_p & have_q) | _ --  and q build p ∧ q option
        | inr have_r => exact _ | have_r            --  else use r
      | inr have_r => exact _ | have_r              -- If no p but r, use that

-- 2.3.2.13
lemma disjunctive_impl {p q r : Prop} : (p ∨ q) → r ≡ (p → r) ∧ (q → r) := by
  constructor

  intro either_impl            -- Proof of (p ∨ q) → r ⊢ (p → r) ∧ (q → r)
  have p_to_r : p → r := by                 -- Construct p → r
    intro have_p                            --  by assuming p
    have valid_input : p ∨ q := have_p | _  --  using it to construct p ∨ q
    apply either_impl valid_input           --  and using that to prove r
  have q_to_r : q → r := by                 -- Construct q → r similarly
    intro have_q
    have valid_input : p ∨ q := _ | have_q
    apply either_impl valid_input
  exact p_to_r & q_to_r                     -- Combine to construct rhs

  intro pair_of_impls          -- Proof of (p → r) ∧ (q → r) ⊢ (p ∨ q) → r
  cases pair_of_impls with
    | intro p_to_r q_to_r =>                -- Split into two implications
      intro p_or_q                          -- Prove → by assuming p ∨ q,
      cases p_or_q with                     --  splitting on whether p or q
        | inl have_p => apply p_to_r have_p --  and using either to prove r
        | inr have_q => apply q_to_r have_q

-- 2.3.2.14
lemma implied_conjunction {p q r : Prop} : p → (q ∧ r) ≡ (p → q) ∧ (p → r) := by
  constructor

  intro impl_both                    -- Proof of (p ∨ q) → r ⊢ (p → r) ∧ (q → r)
  have p_to_q : p → q := by          -- Prove p → q
    intro have_p                     --  by assuming p
    have q_and_r := impl_both have_p --  construct q ∧ r via mp
    cases q_and_r with
      | intro have_q have_r =>       -- Split q ∧ r to get q
        exact have_q
  have p_to_r : p → r := by          -- Prove p → r similarly
    intro have_p
    have q_and_r := impl_both have_p
    cases q_and_r with
      | intro have_q have_r =>
        exact have_r

  exact p_to_q & p_to_r              -- Combine both to prove conjunction

  intro pair_of_impls                -- Proof of (p ∨ q) → r ⊢ (p → r) ∧ (q → r)
  intro have_p                       -- Prove implication by assuming p
  cases pair_of_impls with
    | intro p_to_q p_to_r =>         -- Split premisse into two implications
      have have_q := p_to_q have_p   -- Use mp on p to get q
      have have_r := p_to_r have_p   --  and r,
      exact have_q & have_r          --  construct conjunction

-- 2.3.2.15
lemma curry {p q r : Prop} : p → (q → r) ≡ (p ∧ q) → r := by
  constructor

  intro curried                     -- Proof for p → (q → r) ⊢ (p ∧ q) → r
  intro p_and_q                     -- Proof by assuming p ∧ q
  cases p_and_q with                -- Split into p and q to apply mp twice
    | intro have_p have_q => exact curried have_p $ have_q

  intro uncurried                   -- Proof for (p ∧ q) → r ⊢ p → (q → r)
  intro have_p have_q               -- Assume p, then assume q,
  exact uncurried $ have_p & have_q --  construct p ∧ q and apply mp

-- 2.3.2.16ab
lemma truth_disjunction {p : Prop} : p ∨ ¬p ≡ p ∨ ⊤ := by
  constructor

  intro excl_middle                           -- Proof of p ∨ ¬p ⊢ p ∨ ⊤
  cases excl_middle with
    | inl have_p => exact have_p | _          -- If we have p, pick that
    | inr not_p  => exact      _ | True.intro --  if not, pick truth

  intro p_or_truth                            -- Proof of p ∨ ⊤ ⊢ p ∨ ¬p
  cases p_or_truth with
    | inl have_p => exact have_p | _          -- If we have p, pick that
    | inr top    => exact ax_excl_middle p    --  if not, invoke as axiom

-- 2.3.2.16ac
lemma excluded_middle {p : Prop} : p ∨ ¬p ≡ ⊤ := by
  constructor

  intro                  -- Proof of p ∨ ¬p ⊢ ⊤
  exact True.intro       --  doesn't require assumption, as anything ⊢ ⊤

  intro                  -- Proof of ⊤ ⊢ p ∨ ¬p
  exact ax_excl_middle p -- Invoke as axiom, as it is not provable in IL

-- 2.3.2.16bc
lemma truth_idempotent {p : Prop} : p ∨ ⊤ ≡ ⊤ := by
  constructor

  intro
  exact True.intro

  intro h
  rw [
    ← excluded_middle,
    truth_disjunction
  ] at h
  exact h

-- 2.3.2.17ab
lemma falsity_conjunction {p : Prop} : p ∧ ¬p ≡ p ∧ ⊥ := by
  constructor

  intro paradox                             -- Proof of p ∧ ¬p ⊢ p ∧ ⊥
  cases paradox with
    | intro have_p not_p => contradiction   -- Split into contradiction p and ¬p

  intro p_and_bottom                        -- Proof of p ∧ ⊥ ⊢ p ∧ ¬p
  cases p_and_bottom with                   -- Extract ⊥
    | intro have_p falsity => contradiction --  ⊥ ⊢ anything

-- 2.3.2.17ac
lemma explosion {p : Prop} : p ∧ ¬p ≡ ⊥ := by
  constructor

  intro paradox                             -- Proof of p ∧ ¬p ⊢ ⊥
  cases paradox with
    | intro have_p not_p => contradiction   -- Same as above

  intro absurd                              -- ⊥ ⊢ anything
  contradiction

-- 2.3.2.17bc
lemma falsity_idempotent {p : Prop} : p ∧ ⊥ ≡ ⊥ := by
    constructor

    intro h
    rw [
      ← falsity_conjunction,
      explosion
    ] at h
    exact h

    intro h
    rw [
      ← falsity_conjunction,
      explosion
    ]
    exact h
