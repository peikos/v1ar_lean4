import ArEquiv

-- Equivalence 2025
example {p q : Prop} : (((p → q) → p) → p) ≡ ⊤ := by
  constructor

  intro
  exact True.intro

  intro h
  rw [
    ← impl_intro,
    nonimpl,
    ← impl_intro,
    or_and_dist,
    @or_commute (¬p) p,
    excluded_middle,
    ← truth_conjunction,
    or_commute,
    ← or_associative,
    excluded_middle,
    or_commute,
    truth_idempotent,
  ]
  exact h

-- Reparation Equivalence 2025
example {p q r: Prop} : (p → q) → ((r → p) → (r → q)) ≡ ⊤ := by
  constructor

  intro
  exact True.intro

  intro h
  rw [
    ← impl_intro,
    nonimpl,
    ← impl_intro,
    nonimpl,
    ← impl_intro,
    @or_and_dist r,
    ← or_assoc,
    excluded_middle,
    @or_commute (⊤),
    truth_idempotent,
    @and_commute (⊤),
    ← truth_conjunction,
    or_and_dist,
    ← or_assoc,
    excluded_middle,
    or_commute,
    truth_idempotent,
    ← @or_assoc ¬p,
    @or_commute (¬p ∨ ¬r),
    ← or_assoc,
    @or_commute ¬q,
    excluded_middle,
    or_commute,
    truth_idempotent,
    ← and_idem
  ]
  exact h



-- Reparation Equivalence 2024
example {p q r : Prop} : (p ∨ q) ≡ ((¬p ∧ ¬q) → r) ∧ ((r → p) ∨ q) := by
  constructor
  intro h
  rw [
    ← @or_false p,
    ← @explosion r,
    @or_comm p,
    or_and_dist,
    or_and_dist,
    or_assoc,
    impl_intro,

    @double_negation_intro p,
    @double_negation_intro q,

    ← deMorgan2,

    @or_comm ,
    impl_intro,
    ← @double_negation_intro p,
    ← @double_negation_intro,

  ] at h
  exact h

  intro h
  rw [
    ← impl_intro,
    ← impl_intro,
    ← deMorgan1,
    ← double_negation_intro,
    or_comm,
    or_associative,
    ← or_and_dist,
    explosion,
    or_comm,
    or_false
  ] at h
  exact h

-- Not quite what I envisioned
example {p q r : Prop} : (p → q) ∧ (r → ¬q) → ¬( (q ∧ r) ∨ (p ∧ (q → r))) := by
  have obv {a b : Prop} : (a → b) ∧ (a ∧ b) ↔ a ∧ b := by
    constructor

    intro ⟨ _, a_and_b ⟩
    exact a_and_b

    intro a_and_b
    constructor
    intro
    exact a_and_b.right
    exact a_and_b

  intro h

  rw [@contrapose r ¬q,
      ← double_negation_intro,
      ← impl_intro,
      ← impl_intro,
      and_or_dist,
      and_commute,
      and_or_dist,
      @and_commute q,
      and_or_dist,
      or_commute,
      @and_commute ¬q,
      explosion,
      @or_commute False,
      or_false,
      ← and_or_dist,
      ← deMorgan2,
      and_comm,
      ← nonimpl,
      ← deMorgan1,
      ← deMorgan2,
      or_comm,
      and_comm,
      and_or_dist,
      @and_comm (q ∧ r),
      obv,
      or_comm,
     ] at h
  exact h
