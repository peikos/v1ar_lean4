import Mathlib.Data.Set.Basic
import Mathlib.Tactic.Basic

open Set

-- Cards by position
inductive Card : Type where
  | A1 : Card
  | A2 : Card
  | A3 : Card
  | A4 : Card
  | B1 : Card
  | B2 : Card
  | B3 : Card
  | B4 : Card
  | C1 : Card
  | C2 : Card
  | C3 : Card
  | C4 : Card

-- Sets
axiom Single : Set Card
axiom Double : Set Card
axiom Triple : Set Card
axiom Blue : Set Card
axiom Green : Set Card
axiom Pink : Set Card

-- Colours are mutually exclusive
axiom only_blue : ∀ { x : Card }, x ∈ Blue → x ∉ Green ∧ x ∉ Pink
axiom only_green : ∀ { x : Card }, x ∈ Green → x ∉ Pink ∧ x ∉ Blue
axiom only_pink : ∀ { x : Card }, x ∈ Pink → x ∉ Blue ∧ x ∉ Green

-- Numbers are mutually exclusive
axiom only_single : ∀ { x : Card }, x ∈ Single → x ∉ Double ∧ x ∉ Triple
axiom only_double : ∀ { x : Card }, x ∈ Double → x ∉ Single ∧ x ∉ Triple
axiom only_triple : ∀ { x : Card }, x ∈ Triple → x ∉ Single ∧ x ∉ Double

-- Define row and column for each card for use in spatial relations
def row (c : Card) : Fin 3 :=
  match c with
    | Card.A1 | Card.A2 | Card.A3 | Card.A4 => 0
    | Card.B1 | Card.B2 | Card.B3 | Card.B4 => 1
    | Card.C1 | Card.C2 | Card.C3 | Card.C4 => 2

def col (c : Card) : Fin 4 :=
  match c with
    | Card.A1 | Card.B1 | Card.C1 => 0
    | Card.A2 | Card.B2 | Card.C2 => 1
    | Card.A3 | Card.B3 | Card.C3 => 2
    | Card.A4 | Card.B4 | Card.C4 => 3

def adj3 (x y : Fin 3) :=
  match x with
  | 1 => y ≠ 1
  | 0 | 2 => y = 1

def adj4 (x y : Fin 4) :=
  match x with
  | 1 => y = 2
  | 2 => y = 1 ∨ y = 3
  | 3 => y = 2 ∨ y = 4
  | 4 => y = 3

-- Spatial relations defined in terms of row and column
def Above ( x y : Card ) := row x < row y
def Below ( x y : Card ) := row x > row y
def LeftOf ( x y : Card ) := col x < col y
def RightOf ( x y : Card ) := col x > col y
def Adj ( x y : Card ) := (row x = row y ∧ (adj4 (col x) (col y)))
                        ∨ (col x = col y ∧ (adj3 (row x) (row y)))

-- Set membership
variable (a1_colour : Card.A1 ∈ Pink)
variable (a2_colour : Card.A2 ∈ Pink)
variable (a3_colour : Card.A3 ∈ Green)
variable (a4_colour : Card.A4 ∈ Blue)
variable (b1_colour : Card.B1 ∈ Pink)
variable (b2_colour : Card.B2 ∈ Blue)
variable (b3_colour : Card.B3 ∈ Green)
variable (b4_colour : Card.B4 ∈ Blue)
variable (c1_colour : Card.C1 ∈ Blue)
variable (c2_colour : Card.C2 ∈ Blue)
variable (c3_colour : Card.C3 ∈ Blue)
variable (c4_colour : Card.C4 ∈ Blue)

variable (a1_number : Card.A1 ∈ Triple)
variable (a2_number : Card.A2 ∈ Single)
variable (a3_number : Card.A3 ∈ Single)
variable (a4_number : Card.A4 ∈ Single)
variable (b1_number : Card.B1 ∈ Triple)
variable (b2_number : Card.B2 ∈ Double)
variable (b3_number : Card.B3 ∈ Triple)
variable (b4_number : Card.B4 ∈ Single)
variable (c1_number : Card.C1 ∈ Triple)
variable (c2_number : Card.C2 ∈ Triple)
variable (c3_number : Card.C3 ∈ Triple)
variable (c4_number : Card.C4 ∈ Double)









-- Ex1: False; counterexample x = A1
lemma ex2025a : ¬∀ x, Green x ∨ Blue x := by
  intro contra
  cases contra Card.A1 with
    | inl a1_green => have a1_not_green : Card.A1 ∉ Green := (only_pink a1_colour).right
                      contradiction
    | inr a1_blue   => have a1_not_green : Card.A1 ∉ Blue := (only_pink a1_colour).left
                       contradiction


-- Ex2: True; example x = B4, y = C4
lemma ex2025b : ∃ x y : Card, Adj x y ∧ Blue x ∧ Blue y := by
  apply Exists.intro Card.B4
  apply Exists.intro Card.C4
  constructor

  apply Or.inr
  constructor
  rfl
  rw [adj3]
  have rb4 : row Card.B4 = 1 := rfl
  have rc4 : row Card.C4 = 2 := rfl
  rw [rb4, rc4]
  simp

  constructor
  exact b4_colour
  exact c4_colour


-- Ex3: True; example x = B3, y = B4
lemma ex2025c : ∃ x : Card, ((∃ y : Card, RightOf y x) ∧ ∀ z : Card, LeftOf x z → (Blue z ∨ Single z)) := by
  apply Exists.intro Card.B3
  constructor
  apply Exists.intro Card.B4

  constructor

  intro card
  match card with

  -- Not right of B3
  | Card.A1 | Card.A2 | Card.A3 | Card.B1 | Card.B2 | Card.B3 | Card.C1 | Card.C2 | Card.C3 =>
    intro
    contradiction

  | Card.A4 =>
    intro
    apply Or.inl
    exact a4_colour

  | Card.B4 =>
    intro
    apply Or.inl
    exact b4_colour

  | Card.C4 =>
    intro
    apply Or.inl
    exact c4_colour



-- Ex4: False; counterexample x = C1
lemma ex2025d : ¬∀ x : Card, Triple x → ∃ y : Card, Single y ∧ Adj x y := by
  intro contra

  have counterexample := contra Card.C1 c1_number

  revert counterexample
  contrapose
  intro
  simp

  intro card
  match card with

  -- Different row/column
  | Card.A2 | Card.A3 | Card.A4 | Card.B2 | Card.B3 | Card.B4 =>
    intro
    rw [Adj, not_or]
    constructor
    rw [not_and_or]
    constructor
    intro
    contradiction
    rw [not_and_or]
    constructor
    intro
    contradiction

  | Card.A1 =>
    intro
    have a1_not_single := (only_triple a1_number).left
    contradiction

  | Card.B1 =>
    intro
    have b1_not_single := (only_triple b1_number).left
    contradiction

  | Card.C1 =>
    intro
    have c1_not_single := (only_triple c1_number).left
    contradiction

  | Card.C2 =>
    intro
    have c2_not_single := (only_triple c2_number).left
    contradiction

  | Card.C3 =>
    intro
    have c3_not_single := (only_triple c3_number).left
    contradiction

  | Card.C4 =>
    intro
    have c4_not_single := (only_double c4_number).left
    contradiction



-- Rep1: True
lemma ex2025a_rep : ∀ x : Card, Double x → Blue x := by
  intro card
  match card with

  | Card.A1 =>
    intro
    have a1_not_double : Card.A1 ∉ Double := (only_triple a1_number).right
    contradiction

  | Card.A2 =>
    intro
    have a2_not_double : Card.A2 ∉ Double := (only_single a2_number).left
    contradiction

  | Card.A3 =>
    intro
    have a3_not_double : Card.A3 ∉ Double := (only_single a3_number).left
    contradiction

  | Card.A4 =>
    intro
    exact a4_colour

  | Card.B1 =>
    intro
    have b1_not_double : Card.B1 ∉ Double := (only_triple b1_number).right
    contradiction

  | Card.B2 =>
    intro
    exact b2_colour

  | Card.B3 =>
    intro
    have b3_not_double : Card.B3 ∉ Double := (only_triple b3_number).right
    contradiction

  | Card.B4 =>
    intro
    exact b4_colour

  | Card.C1 =>
    intro
    exact c1_colour

  | Card.C2 =>
    intro
    exact c2_colour

  | Card.C3 =>
    intro
    exact c3_colour

  | Card.C4 =>
    intro
    exact c4_colour



-- Rep2: False
lemma ex2025b_rep : ¬∃ x y : Card, LeftOf x y ∧ Green x ∧ Pink y := by
  rw [not_exists]

  intro card_1
  rw[not_exists]

  match card_1 with

  | Card.A1 =>
    intro card_2
    rw [and_comm, and_assoc, not_and, not_and]
    intro h
    have not_green := (only_pink a1_colour).right
    contradiction

  | Card.A2 =>
    intro card_2
    rw [and_comm, and_assoc, not_and, not_and]
    intro h
    have not_green := (only_pink a2_colour).right
    contradiction

  | Card.A3 =>
    intro card_2
    rw [and_comm, and_assoc, not_and, not_and]
    intro _ card2_pink

    have a4_not_pink := (only_blue a4_colour).right
    have b4_not_pink := (only_blue b4_colour).right
    have c4_not_pink := (only_blue c4_colour).right

    cases card_2 <;> intro h <;> contradiction

  | Card.A4 =>
    intro card_2
    rw [and_comm, and_assoc, not_and, not_and]
    intro h
    have not_green := (only_blue a4_colour).left
    contradiction

  | Card.B1 =>
    intro card_2
    rw [and_comm, and_assoc, not_and, not_and]
    intro h
    have not_green := (only_pink b1_colour).right
    contradiction

  | Card.B2 =>
    intro card_2
    rw [and_comm, and_assoc, not_and, not_and]
    intro h
    have not_green := (only_blue b2_colour).left
    contradiction

  | Card.B3 =>
    intro card_2
    rw [and_comm, and_assoc, not_and, not_and]
    intro _ card2_pink

    have a4_not_pink := (only_blue a4_colour).right
    have b4_not_pink := (only_blue b4_colour).right
    have c4_not_pink := (only_blue c4_colour).right

    cases card_2 <;> intro h <;> contradiction

  | Card.B4 =>
    intro card_2
    rw [and_comm, and_assoc, not_and, not_and]
    intro h
    have not_green := (only_blue b4_colour).left
    contradiction

  | Card.C1 =>
    intro card_2
    rw [and_comm, and_assoc, not_and, not_and]
    intro h
    have not_green := (only_blue c1_colour).left
    contradiction

  | Card.C2 =>
    intro card_2
    rw [and_comm, and_assoc, not_and, not_and]
    intro h
    have not_green := (only_blue c2_colour).left
    contradiction

  | Card.C3 =>
    intro card_2
    rw [and_comm, and_assoc, not_and, not_and]
    intro h
    have not_green := (only_blue c3_colour).left
    contradiction

  | Card.C4 =>
    intro card_2
    rw [and_comm, and_assoc, not_and, not_and]
    intro h
    have not_green := (only_blue c4_colour).left
    contradiction




-- Rep3: True; example x = C2
lemma ex2025c_rep : ∃ x : Card, (∀ y : Card, Adj x y → Blue y ∨ (Single y ∧ ∃ z : Card, Adj z y ∧ Pink z)) := by
  apply Exists.intro Card.C2
  intro card_1
  rw [
    imp_iff_not_or
  ]
  match card_1 with

  -- Different row/column
  | Card.A1 | Card.A3 | Card.A4 | Card.B1 | Card.B3 | Card.B4 =>
    rw [Adj, not_or]
    apply Or.inl
    rw [not_and_or]
    constructor
    apply Or.inl
    intro
    contradiction
    rw [not_and]
    intro
    contradiction

  | Card.A2 =>
    have rc2 : row Card.C2 = 2 := rfl
    have ra2 : row Card.A2 = 0 := rfl
    rw [Adj, not_or]
    apply Or.inl
    rw [not_and_or]
    constructor
    apply Or.inl
    intro
    contradiction
    rw [not_and]
    intro
    rw [adj3, rc2, ra2]
    simp

  | Card.C2 =>
    have rc2 : row Card.C2 = 2 := rfl
    have cc2 : col Card.C2 = 1 := rfl
    rw [Adj, not_or]
    apply Or.inl
    rw [not_and_or]
    constructor
    apply Or.inr
    rw [adj4, cc2]
    intro
    contradiction
    rw [not_and]
    intro
    rw [adj3, rc2]
    simp


  | Card.C4 =>
    have cc2 : col Card.C2 = 1 := rfl
    have cc4 : col Card.C4 = 3 := rfl
    rw [Adj, not_or]
    apply Or.inl
    rw [not_and_or]
    constructor
    apply Or.inr
    rw [adj4, cc2, cc4]
    simp
    rw [adj3, cc2, cc4]
    simp

  | Card.B2 =>
    rw [Adj]
    apply Or.inr
    apply Or.inl
    exact b2_colour

  | Card.C1 =>
    rw [Adj]
    apply Or.inr
    apply Or.inl
    exact c1_colour

  | Card.C3 =>
    apply Or.inr
    constructor
    exact c3_colour




-- Rep4: False; counterexample x = B2
lemma ex2025d_rep : ¬∀ x : Card, Double x → ¬∃ y : Card, Adj x y ∧ Green y := by
  simp
  apply Exists.intro Card.B2
  constructor
  exact b2_number

  apply Exists.intro Card.B3
  constructor

  rw [Adj]

  apply Or.inl

  constructor
  rfl

  rfl

  exact b3_colour
