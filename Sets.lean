import Mathlib.Data.Set.Basic
import Mathlib.Tactic.Basic

open Set

variable {U : Type}
variable (A B C : Set U)
variable (x : U)

notation:10 a "Δ" b => symmDiff a b


-- Counterexample for ex2025
variable (n : U)
variable (n_not_in_A : n ∉ A)
variable (n_not_in_B : n ∉ B)
variable (n_not_in_C : n ∉ C)

lemma ex2025 : (A Δ B) \ C ⊂ ((A ∩ B) ∪ C)ᶜ \ ((C ∩ B) ∪ (C ∩ A)) := by
  rw [
    ssubset_iff_of_subset
  ]

  -- Proof ∃ x ∈ superset, x ∉ subset
  apply Exists.intro n
  simp

  constructor
  constructor
  constructor

  intro
  exact n_not_in_B
  exact n_not_in_C

  intro contra
  rw [ ← and_or_left ] at contra
  have n_in_c : n ∈ C := contra.left
  contradiction

  intro h
  cases h with
    | inl n_in_a_not_b =>
      have n_in_a : n ∈ A := n_in_a_not_b.left
      contradiction
    | inr n_in_b_not_a =>
      have n_in_b : n ∈ B := n_in_b_not_a.left
      contradiction

  -- Proof of  subset ⊆ superset
  intro x

  intro h
  simp
  rw [ symmDiff_sdiff ] at h
  simp at h
  rw [
    not_or,
    not_or,
    ← and_assoc,
    ← and_assoc,
    ← or_and_right
    ] at h

  constructor
  constructor

  intro x_in_a
  cases h.left with
    | inl only_a => exact only_a.right
    | inr only_b => have x_not_in_a := only_b.right
                    contradiction

  exact h.right

  intro contra
  rw [ ← and_or_left ] at contra
  have x_in_c := contra.left
  have x_not_in_c := h.right
  contradiction


lemma ex2025h : ((C ∩ B) ∪ (C ∩ A)) \ ((A ∩ B) ∪ C)ᶜ = (A ∪ B) ∩ C := by
  ext x
  apply Iff.intro
  simp

  rw [
    @or_comm (x ∈ A),
    ←and_or_left
  ]

  intro h
  intro
  apply And.intro
  have x_in_b_or_a : x ∈ B ∨ x ∈ A := h.right
  exact x_in_b_or_a
  have x_in_c : x ∈ C := h.left
  exact x_in_c

  intro h
  simp
  rw [@or_comm, ← and_or_left]

  apply And.intro

  have x_in_c : x ∈ C := h.right
  have x_in_a_or_b : x ∈ A ∨ x ∈ B := h.left
  apply And.intro
  exact x_in_c
  exact x_in_a_or_b

  intro
  exact h.right
